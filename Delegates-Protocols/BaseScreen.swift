//
//  BaseScreen.swift
//  Delegates-Protocols
//
//  Created by Juan Manuel García Carmona @jcarmonamx on 24/11/21.
//

import UIKit

class BaseScreen: UIViewController {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var chooseButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        chooseButton.layer.cornerRadius = chooseButton.frame.size.height/2
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func chooseButtonTapped(_ sender: UIButton) {
        let selectionVC = storyboard?.instantiateViewController(withIdentifier: "SelectionScreen") as! SelectionScreen
        selectionVC.selectionDelegate = self
        present(selectionVC, animated: true, completion: nil)
    }
    
    
}

/*
 * MARK: SideSelectionDelegate
 */
extension BaseScreen: SideSelectionDelegate {
    
    /*
     * Funcion que se debe definir para cumplir con el contrato del selectionDelegate
     * esta se mandara llamar desde la clase que implementa el protocolo (SelectionScreen)
     * Permitiendo que desde la clase selectionDelegate, se puedan cambiar las propiedades
     * de la imagen, la etiqueta y el fondo de la vista
     */
    func didTapChoice(image: UIImage, name: String, color: UIColor) {
        mainImageView.image = image
        nameLabel.text = name
        view.backgroundColor = color
    }
    
}
