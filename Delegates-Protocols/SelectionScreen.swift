//
//  SelectionScreen.swift
//  Delegates-Protocols
//
//  Created by Juan Manuel García Carmona @jcarmonamx on 24/11/21.
//

import UIKit

/*
 * MARK: Delegte
 * Delegado que implementara la clase BaseScreen
 */
protocol SideSelectionDelegate {
    func didTapChoice(image: UIImage, name: String, color: UIColor)
}

class SelectionScreen: UIViewController {
    
    /*
     * MARK: Variables
     */
    
    var selectionDelegate: SideSelectionDelegate!
    
    /*
     * MARK: Funciones del sistema
     */
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    /*
     * MARK: Actions
     */
    
    @IBAction func imperialButtonTapped(_ sender: UIButton) {
        // llamamos la funcion del delegado que va a manipular el contenido de la clase BaseScreen
        selectionDelegate.didTapChoice(image: UIImage(named: "vader")!, name: "Darth Vader", color: .red)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func rebelButtonTapped(_ sender: UIButton) {
        // llamamos la funcion del delegado que va a manipular el contenido de la clase BaseScreen
        selectionDelegate.didTapChoice(image: UIImage(named: "luke")!, name: "Luke Skywalker", color: .cyan)
        dismiss(animated: true, completion: nil)
    }
    
}
